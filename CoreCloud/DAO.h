//
//  DAO.h
//  CoreCloud
//
//  Created by Водолазкий В.В. on 11/03/2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "DemoEntity+CoreDataClass.h"

@interface DAO : NSObject

- (instancetype) initWithMOC:(NSManagedObjectContext *)moc;

/**
 	Deletes all data from the database, in the document
 */
- (void) clearDatabase;

/**
  * Provides access to all records in the database
 */





@property (nonatomic, retain) NSManagedObjectContext *moc;

@end
