//
//  DemoEntity+CoreDataClass.m
//  CoreCloud
//
//  Created by Водолазкий В.В. on 11/03/2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//
//

#import "DemoEntity+CoreDataClass.h"

@implementation DemoEntity

- (void) awakeFromInsert
{
	self.testDate = [NSDate date];
	NSDateFormatter *df = [[NSDateFormatter alloc] init];
	df.dateFormat = @"YYYY-MMM-dd hh:mm:ss";
	self.testString = [NSString stringWithFormat:@"Test string - %@",[df stringFromDate:self.testDate]];
}

- (NSString *) description
{
	return [NSString stringWithFormat:@"DemoEntity: %@", self.testString];
}


@end
