//
//  DemoEntity+CoreDataProperties.h
//  CoreCloud
//
//  Created by Водолазкий В.В. on 11/03/2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//
//

#import "DemoEntity+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface DemoEntity (CoreDataProperties)

+ (NSFetchRequest<DemoEntity *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *testString;
@property (nullable, nonatomic, copy) NSDate *testDate;

@end

NS_ASSUME_NONNULL_END
