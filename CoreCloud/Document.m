//
//  Document.m
//  CoreCloud
//
//  Created by Водолазкий В.В. on 11/03/2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "Document.h"
#import "DAO.h"

@interface Document ()
@property (strong) IBOutlet NSArrayController *tableArrayController;
@property (weak) IBOutlet NSTableView *tableView;


@property (weak) IBOutlet NSButton *clearButton;

- (IBAction)clearButtonClicked:(id)sender;


@end

@implementation Document

- (instancetype)init {
    self = [super init];
    if (self) {
		// Add your subclass-specific initialization here.
    }
    return self;
}

+ (BOOL)autosavesInPlace {
	return YES;
}


- (void) windowControllerDidLoadNib:(NSWindowController *)windowController
{
	self.tableArrayController.managedObjectContext = self.managedObjectContext;
	self.tableView.dataSource = self;
	self.tableView.delegate = self;
	
	
}



- (NSString *)windowNibName {
	// Override returning the nib file name of the document
	// If you need to use a subclass of NSWindowController or if your document supports multiple NSWindowControllers, you should remove this method and override -makeWindowControllers instead.
	return @"Document";
}



- (IBAction)clearButtonClicked:(id)sender
{
	DAO *dao = [[DAO alloc] initWithMOC:self.managedObjectContext];
	[dao clearDatabase];
	[self.tableView reloadData];
}

- (void) saveDocument:(id)sender
{
    [super saveDocument:sender];
    
}
- (void)close
{
    [super close];
    // Delete two files wish extention .wal and .shm
    NSFileManager *fm = [NSFileManager defaultManager];
    for (NSString *extention in @[@"sqlite-wal", @"sqlite-shm"]) {
        NSURL *urlDelete = [self.fileURL.URLByDeletingPathExtension URLByAppendingPathExtension:extention];
        if ([fm fileExistsAtPath:urlDelete.path]) {
            [fm removeItemAtURL:urlDelete error:nil];
        }
    }
}
@end
