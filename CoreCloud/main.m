//
//  main.m
//  CoreCloud
//
//  Created by Водолазкий В.В. on 11/03/2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
	return NSApplicationMain(argc, argv);
}
