//
//  DAO.m
//  CoreCloud
//
//  Created by Водолазкий В.В. on 11/03/2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "DAO.h"

@implementation DAO

@synthesize moc = moc;

- (instancetype) initWithMOC:(NSManagedObjectContext *)moc
{
	if (self = [super init]) {
		self.moc = moc;
	}
	return self;
}

- (void) clearDatabase
{
	NSFetchRequest *req = [[NSFetchRequest alloc] initWithEntityName:[[DemoEntity class] description]];
	NSError *error = nil;
	NSArray <DemoEntity *> *recordsToDelete = [self.moc executeFetchRequest:req error:&error];
	if (!recordsToDelete && error) {
		NSAlert *alert = [NSAlert alertWithError:error];
		[alert runModal];
		return;
	}
	if (recordsToDelete.count > 0) {
		for (DemoEntity *obj in recordsToDelete) {
			[self.moc deleteObject:obj];
		}
	}
}


@end
