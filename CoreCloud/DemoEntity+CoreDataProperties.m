//
//  DemoEntity+CoreDataProperties.m
//  CoreCloud
//
//  Created by Водолазкий В.В. on 11/03/2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//
//

#import "DemoEntity+CoreDataProperties.h"

@implementation DemoEntity (CoreDataProperties)

+ (NSFetchRequest<DemoEntity *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"DemoEntity"];
}

@dynamic testString;
@dynamic testDate;

@end
