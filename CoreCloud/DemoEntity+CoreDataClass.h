//
//  DemoEntity+CoreDataClass.h
//  CoreCloud
//
//  Created by Водолазкий В.В. on 11/03/2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface DemoEntity : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "DemoEntity+CoreDataProperties.h"
