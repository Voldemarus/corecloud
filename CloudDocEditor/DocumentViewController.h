//
//  DocumentViewController.h
//  CloudDocEditor
//
//  Created by Водолазкий В.В. on 13/03/2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Document.h"

@interface DocumentViewController : UIViewController

@property (strong) Document *document;

@end
