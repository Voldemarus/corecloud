//
//  Document.m
//  CloudDocEditor
//
//  Created by Водолазкий В.В. on 13/03/2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "Document.h"

NSString * const VVVDocumentDataReloaded	=	@"VVVDocumentDataReloaded";

@interface Document () {
	NSManagedObjectContext *moc;
}

@property (nonatomic, retain) NSPersistentContainer *container;
@property (nonatomic, retain) NSFetchedResultsController *frc;

@end


@implementation Document

@synthesize  frc = frc;


//
// Called when document is about to be saved. n our case we just copy content
// of the local Core Data base (sqlite) into destination file.
//
//  1. Save local context
// 2.  Release persistnt store (referred to local file
// 3.  Ini NSData with content of this file
// 4. return this NSData as content of the destination file
//
- (id)contentsForType:(NSString*)typeName error:(NSError **)errorPtr {
    // Encode your document with an instance of NSData or NSFileWrapper
	NSError *error = nil;
	if ([moc hasChanges]) {
		[moc save:&error];
		if (error) {
			NSLog(@"Unresolved error %@, %@", error, error.userInfo);
			abort();
		}
	}
	// Clear all persistentStores
	for (NSPersistentStore *store in _container.persistentStoreCoordinator.persistentStores) {
		[_container.persistentStoreCoordinator removePersistentStore:store error:nil];
	}

	return [[NSData alloc] initWithContentsOfURL:self.fileURL];
}


//
// Method is called when document is opened. Core Data on IOS allows to
// use files either in bundle ir Library (read-only) or in Documents folder
// Therefore we create copy of source file in Docuent directory and set up
// NSPersistemtContainer.
//

- (BOOL)loadFromContents:(id)contents ofType:(NSString *)typeName error:(NSError **)errorPtr {
	
    return [self initPersistentContainerWothStoreFile:self.fileURL];

 }

#pragma mark - content access -

//
// These methods are helpers to limit all access to actual records in the
// Documrnt.
//

- (NSInteger) recordCount
{
	return frc.fetchedObjects.count;
}

- (BOOL) reloadData
{
	NSError *error = nil;
	[frc performFetch:&error];
	return (error == nil);
}

- (DemoEntity *) entityForIndex:(NSInteger)index
{
	if (index < 0 || index >= self.recordCount) {
		return nil;
	}
	return [[frc fetchedObjects] objectAtIndex:index];
}

- (void) createNewRecord
{
	// Create new Cell!
	DemoEntity *newRecord = [NSEntityDescription insertNewObjectForEntityForName:[[DemoEntity class] description] inManagedObjectContext:moc];
#pragma unused (newRecord)
}



#pragma mark - Core Data stack

//
// Inintialisation of the NSPersistemtContainer
//

- (BOOL) initPersistentContainerWothStoreFile:(NSURL *)storeFile
{
	if (!storeFile) {
		return NO;
	}
	
	@synchronized (self) {
		// See model file name!
		_container = [[NSPersistentContainer alloc] initWithName:@"Document"];
		
		NSPersistentStoreDescription *psd = [[NSPersistentStoreDescription alloc] initWithURL:storeFile];
		
		_container.persistentStoreDescriptions = @[psd];
		[_container loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
#ifdef DEBUG
			NSLog(@"Container descriptors");
			for (NSPersistentStoreDescription *ps in _container.persistentStoreDescriptions) {
				NSLog(@"type - %@", ps.type);
				NSLog(@"configuration - %@", ps.configuration);
				NSLog(@"URL - %@", ps.URL);
				NSLog(@"options - %@",ps.options);
                NSLog(@"storeFile = %@",storeFile);
				NSLog(@" ");
			}
#endif
			
			if (error != nil) {
				NSLog(@"Unresolved error %@, %@", error, error.userInfo);
				abort();
			}
			moc = _container.viewContext;        // Usually is used for display data
			if (moc) {
				NSFetchRequest *req = [[NSFetchRequest alloc] initWithEntityName:[[DemoEntity class] description]];
				req.sortDescriptors = @[
										[NSSortDescriptor sortDescriptorWithKey:@"testDate" ascending:NO],
										];
				frc = [[NSFetchedResultsController alloc] initWithFetchRequest:req managedObjectContext:moc sectionNameKeyPath:nil cacheName:nil];
				NSError *error = nil;
				[frc performFetch:&error];
				if (error) {
					NSLog(@"Cannot perform fetch from database - %@",[error localizedDescription]);
				} else {
					dispatch_async(dispatch_get_main_queue(), ^{
						[[NSNotificationCenter defaultCenter] postNotificationName:VVVDocumentDataReloaded object:nil];
					});
				}
			}
		}];
	}
	return YES;
}

@end
