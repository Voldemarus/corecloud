//
//  DocumentViewController.m
//  CloudDocEditor
//
//  Created by Водолазкий В.В. on 13/03/2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "DocumentViewController.h"
#import <CoreData/CoreData.h>
#import "DemoEntity+CoreDataClass.h"

#import "AddNewRowTableViewCell.h"
#import "DemoEntityTableViewCell.h"

#import "Document.h"

@interface DocumentViewController() <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UINavigationItem *documentName;
- (IBAction)closeDocument:(id)sender;

@end

@implementation DocumentViewController


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	
    // Register cells for the table
    [self.tableView registerNib:[AddNewRowTableViewCell cellNib] forCellReuseIdentifier:AddNewRowTableViewCellReusableID];
    [self.tableView registerNib:[DemoEntityTableViewCell cellNib] forCellReuseIdentifier:DemoEntityTableViewCellReusableID];

	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc addObserver:self selector:@selector(updateTableView:) name: VVVDocumentDataReloaded  object:nil];
	
	self.tableView.delegate = self;
	self.tableView.dataSource = self;
	
    // Access the document
    [self.document openWithCompletionHandler:^(BOOL success) {
        if (success) {
			// initial view initialising can be placed here. But actual initialisation
			// i performed in async mode, so tableview will be refreshed by
			// notification.
        }
    }];
    
}


//
// When user taps Close button we should save document first then close it
// before document view id dismissed
//
- (IBAction)closeDocument:(id)sender
{
    [self.document.sourceURL startAccessingSecurityScopedResource]; // Permision fo change file in Cloud
    [self.document saveToURL:self.document.sourceURL forSaveOperation:UIDocumentSaveForOverwriting completionHandler:^(BOOL successSave) {
        //if (success1) {
        //    NSLog(@"Document saved!!!");
        //} else {
        //    NSLog(@"‼️ cannot save!!!");
        //}
        [self.document closeWithCompletionHandler:^(BOOL successClose) {
            [self.document.fileURL stopAccessingSecurityScopedResource]; // pair start/stop
            
            NSLog(@"%@ Document %@ %@. %@",(successSave&successClose)?@"❇️":@"‼️" , self.document.fileURL.lastPathComponent, successSave?@"saved":@"cannot save", successClose?@"Closed":@"Not closed");
            
            // ---  Delete two files wish extention .wal and .shm
            NSFileManager *fm = [NSFileManager defaultManager];
            for (NSString *extention in @[@"sqlite-wal", @"sqlite-shm"]) {
                NSURL *urlDelete = [self.document.fileURL.URLByDeletingPathExtension URLByAppendingPathExtension:extention];                    [fm removeItemAtURL:urlDelete error:nil];
            }
            // ---

            [self dismissViewControllerAnimated:YES completion:^ {
                //
            }];
        }];
    }];
}




- (void) updateTableView:(NSNotification *) note
{
	[self.tableView reloadData];
}

#pragma mark - UITableView Delegate

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
	return 2;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (section == 0) {
		return 1;				// Add new Cell
	} else {
		return self.document.recordCount;
	}
}

- (__kindof UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 0) {
		AddNewRowTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:AddNewRowTableViewCellReusableID];
		// nothing to set on this cell
		return cell;
	}
	//
	// Section 1. We should put there either regular cell
	//
	DemoEntityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:DemoEntityTableViewCellReusableID];
	cell.data = [self.document entityForIndex:indexPath.row];
	return cell;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
	if (indexPath.section == 0) {
		[self.document createNewRecord];
		// all fields are filled automaGically
		if ([self.document reloadData]) {
			[self.tableView reloadData];
		}
	}
}



@end
