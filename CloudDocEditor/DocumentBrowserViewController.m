//
//  DocumentBrowserViewController.m
//  CloudDocEditor
//
//  Created by Водолазкий В.В. on 13/03/2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "DocumentBrowserViewController.h"
#import "Document.h"
#import "DocumentViewController.h"

@interface DocumentBrowserViewController () <UIDocumentBrowserViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end

@implementation DocumentBrowserViewController
    
- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate = self;
    self.allowsDocumentCreation = YES;
    self.allowsPickingMultipleItems = NO;
    
    // Update the style of the UIDocumentBrowserViewController
    // self.browserUserInterfaceStyle = UIDocumentBrowserUserInterfaceStyleDark;
    // self.view.tintColor = [UIColor whiteColor];
    
    // Specify the allowed content types of your application via the Info.plist.
    
    // Do any additional setup after loading the view, typically from a nib.
}

#pragma mark UIDocumentBrowserViewControllerDelegate

- (void)documentBrowser:(UIDocumentBrowserViewController *)controller didRequestDocumentCreationWithHandler:(void (^)(NSURL * _Nullable, UIDocumentBrowserImportMode))importHandler {
	
	//
	// Template file is actually enpty database in the required format.
	// So when new document is created we will just create fresh
	// database suitable to be used in application
	//
	NSURL *newDocumentURL = [[NSBundle mainBundle] URLForResource:@"CloudCoreTemplate" withExtension:@"sqlite"];
	
    // Optionally, you can present a template chooser before calling the importHandler.
    // Make sure the importHandler is always called, even if the user cancels the creation request.
	
    if (newDocumentURL != nil) {
		// We COPY our template file from the app' bundle into selected location
        importHandler(newDocumentURL, UIDocumentBrowserImportModeCopy);
    } else {
        importHandler(newDocumentURL, UIDocumentBrowserImportModeNone);
    }
}

-(void)documentBrowser:(UIDocumentBrowserViewController *)controller didPickDocumentURLs:(NSArray<NSURL *> *)documentURLs {
    NSURL *sourceURL = documentURLs.firstObject;
    if (!sourceURL) {
        return;
    }
    
    // Present the Document View Controller for the first document that was picked.
    // If you support picking multiple items, make sure you handle them all.
    [self presentDocumentAtURL:sourceURL];
}

- (void)documentBrowser:(UIDocumentBrowserViewController *)controller didImportDocumentAtURL:(NSURL *)sourceURL toDestinationURL:(NSURL *)destinationURL {
    // Present the Document View Controller for the new newly created document
    [self presentDocumentAtURL:destinationURL];
}

- (void)documentBrowser:(UIDocumentBrowserViewController *)controller failedToImportDocumentAtURL:(NSURL *)documentURL error:(NSError * _Nullable)error {
    // Make sure to handle the failed import appropriately, e.g., by presenting an error message to the user.
}

// MARK: Document Presentation

- (void)presentDocumentAtURL:(NSURL *)cloudDocumentURL {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DocumentViewController *documentViewController = [storyBoard instantiateViewControllerWithIdentifier:@"DocumentViewController"];
    
    // --- Copy(overwrite) opening file to temporaly directory
    NSFileManager *fm = [NSFileManager defaultManager];
    NSURL * tempURL = [[NSURL fileURLWithPath:NSTemporaryDirectory()] URLByAppendingPathComponent:cloudDocumentURL.lastPathComponent];
    [fm removeItemAtURL:tempURL error:nil];
    [cloudDocumentURL startAccessingSecurityScopedResource]; // set Permissin read file
    if (![fm copyItemAtURL:cloudDocumentURL toURL:tempURL error:nil]) {
        NSLog(@"‼️ Error copy opening file!");
    }
    [cloudDocumentURL stopAccessingSecurityScopedResource];
    // ---
    
    documentViewController.document = [[Document alloc] initWithFileURL:tempURL];
    documentViewController.document.sourceURL = cloudDocumentURL; // for save data after close document
    [self presentViewController:documentViewController animated:YES completion:nil];
}

@end
