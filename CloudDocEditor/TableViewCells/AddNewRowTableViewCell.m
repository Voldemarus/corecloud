//
//  AddNewRowTableViewCell.m
//  QRWallet
//
//  Created by Водолазкий В.В. on 29.07.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "AddNewRowTableViewCell.h"


NSString * const AddNewRowTableViewCellReusableID	=	@"AddNewRowTableViewCellReusableID";

@interface AddNewRowTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *promptLabel;

@end

@implementation AddNewRowTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];

	self.promptLabel.textColor = [UIColor darkGrayColor];
	self.promptLabel.text = @"Add New Record";
   
}

#pragma mark -

+ (CGFloat) cellHeight
{
	return 44.0;
}


@end
