//
//  CommonTableViewCell.h
//  QRWallet
//
//  Created by Водолазкий В.В. on 29.07.17.
//  Copyright © 2017 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CommonTableViewCell : UITableViewCell


+ (UINib *) cellNib;

+ (CGFloat) cellHeight;


@end
