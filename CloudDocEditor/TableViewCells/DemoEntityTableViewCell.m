//
//  DemoEntityTableViewCell.m
//  CloudDocEditor
//
//  Created by Водолазкий В.В. on 13/03/2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "DemoEntityTableViewCell.h"

NSString * const DemoEntityTableViewCellReusableID	=	@"DemoEntityTableViewCellReusableID";


@interface DemoEntityTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *cellTitle;
@property (weak, nonatomic) IBOutlet UILabel *cellDate;

@end

@implementation DemoEntityTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
	_cellTitle.text = @"???";
	_cellDate.text = @"";
}

- (void) setData:(DemoEntity *)data
{
	_data = data;
	_cellTitle.text = _data.testString;
	NSDateFormatter *df = [[NSDateFormatter alloc] init];
	df.dateStyle = NSDateFormatterMediumStyle;
	_cellDate.text = [df stringFromDate:_data.testDate];
}

#pragma mark -

+ (CGFloat) cellHeight
{
	return 44.0;
}


@end
