//
//  DemoEntityTableViewCell.h
//  CloudDocEditor
//
//  Created by Водолазкий В.В. on 13/03/2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import "CommonTableViewCell.h"
#import <CoreData/CoreData.h>
#import "DemoEntity+CoreDataClass.h"


extern NSString * const DemoEntityTableViewCellReusableID;


@interface DemoEntityTableViewCell : CommonTableViewCell

@property (nonatomic, retain) DemoEntity *data;

@end
