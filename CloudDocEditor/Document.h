//
//  Document.h
//  CloudDocEditor
//
//  Created by Водолазкий В.В. on 13/03/2018.
//  Copyright © 2018 Geomatix Laboratory S.R.O. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "DemoEntity+CoreDataClass.h"

extern NSString * const VVVDocumentDataReloaded;

@interface Document : UIDocument

@property (nonatomic, readonly) NSInteger recordCount;
@property (nonatomic, retain) NSURL *sourceURL;

- (void) createNewRecord;
- (BOOL) reloadData;
- (DemoEntity *) entityForIndex:(NSInteger) index;


@end
